@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
              <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>

            <div class="card">
              <div class="card-header">Upload Image</div>
                <div class="card-body">
                  <div class="row justify-content-center">
                      <form class="m-2" method="post" action="/upload" enctype="multipart/form-data">
                          @csrf
                          <div class="form-group">
                              <input type="text" class="form-control" id="name" placeholder="Enter file Name" name="name">
                          </div>
                          <div class="form-group">
                              <label for="image">Choose Image</label>
                              <input id="image" type="file" name="image">
                          </div>
                          <button type="submit" class="btn btn-dark d-block w-75 mx-auto">Upload</button>
                      </form>
                  </div>
                </div>
            </div>

            <div class="card">
              <div class="card-header">Image List (Storage Used Size : {{number_format($total_size)}}
                 <font color='red'>{{$warning_string}}</font>)</div>
                <div class="card-body">
                  @foreach ($images as $image)
                    <img src='/storage/images/{{$image->file_name}}' width='100'/>&nbsp;&nbsp;
                    <b>Size:</b>{{number_format($image->size)}}&nbsp;&nbsp;
                    <b>Mime:</b>{{$image->mime}}&nbsp;&nbsp;
                    <b>Date/Time Created:</b> {{$image->created_at." UTC"}}
                    <br><br>

                  @endforeach
                </div>
              </div>
            </div>

        </div>
    </div>
</div>
@endsection
