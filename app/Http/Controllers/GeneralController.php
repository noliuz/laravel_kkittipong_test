<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input as Input;
use Illuminate\Support\Facades\Storage;
use App\Image;
use App\Http\Controllers\Auth;


class GeneralController extends Controller {

  public function test() {


  }

  public function upload(Request $req) {
    //check extension
    $imageExtension = $req->file('image')->extension();
    if ($imageExtension != 'png' && $imageExtension != 'jpeg'
       ) {
      return redirect('/error/Please upload only png and jpg file.');
    }

    if ($req->file('image')) {
      $imageName = time().$req->file('image')->getClientOriginalName();
      $fileSize = $req->file('image')->getSize();
      $userId = auth()->user()['id'];
      $destinationPath = public_path('/storage/images');
      $req->file('image')->move($destinationPath,$imageName);

      //save file
      $file = new Image;
      $file->file_name = $imageName;
      $file->size = $fileSize;
      $file->mime = $imageExtension;
      $file->user_id = $userId;
      $file->save();

      return redirect('/home');
    }

  }

  public function viewUploads () {
      $images = File::all();
      return view('view_uploads')->with('images', $images);
  }
}
