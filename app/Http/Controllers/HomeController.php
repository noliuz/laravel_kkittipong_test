<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //query image by user id
        $userId = auth()->user()['id'];
        $images = Image::where('user_id',$userId)->get();
        //find total images size
        $total_size = 0;
        foreach ($images as $image) {
          $total_size += $image->size;
        }
        //send mail if total size is over 100mb
        $warningStr = '';
        //if ($total_size >= 10) {
        if ($total_size >= 1073741824*9/10) {
          $warningStr = '(Warning image size over 90%. Mail sent!!)';
          //sending mail
          $email = auth()->user()->email;
            //ผมไม่มีเมล์ server ไว้เทส คงแสดงได้เท่านี้นะครับ ทดลองใช้ sendmail ของ ubuntu แล้ว gmail มันปัดทิ้งไปเลย ต้องไปสมัคร mail server ที่น่าเชื่อถือสำหรับ gmail ครับ ใส่ลิ้ง document ไว้ละกันนะครับ https://laravel.com/docs/8.x/mail
        }

        return view('home',['images'=>$images,
                            'total_size'=>$total_size,
                            'warning_string' => $warningStr
               ]);
    }
}
